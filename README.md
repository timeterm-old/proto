# proto

## Genereren (Go)

```$ make proto-go```

## Dependencies installeren voorbereiden (C++)

```$ conan remote add inexorgame https://api.bintray.com/conan/inexorgame/inexor-conan```  
```$ conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan```

## Genereren (C++)

```$ conan create . user/channel --build TimeTermProto```

Als dit een foutmelding geeft in de orde van `<dependency> is not built for target`, voeg dan `--build <dependency>` toe. Dit is mij gebeurd voor c-ares.

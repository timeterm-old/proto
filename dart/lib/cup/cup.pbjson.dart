///
//  Generated code. Do not modify.
//  source: cup.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const SignInEventType$json = const {
  '1': 'SignInEventType',
  '2': const [
    const {'1': 'SE_SEARCHING_NAME', '2': 0},
    const {'1': 'SE_SELECTING_USER', '2': 1},
    const {'1': 'SE_SIGNING_IN', '2': 2},
    const {'1': 'SE_SIGNED_IN', '2': 3},
    const {'1': 'SE_FAILED', '2': 4},
  ],
};

const SearchUsersRequest$json = const {
  '1': 'SearchUsersRequest',
  '2': const [
    const {'1': 'first_letters', '3': 1, '4': 1, '5': 9, '10': 'firstLetters'},
  ],
};

const SearchUsersResponse$json = const {
  '1': 'SearchUsersResponse',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 9, '10': 'users'},
  ],
};

const ChooseRequest$json = const {
  '1': 'ChooseRequest',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
    const {'1': 'internal_id', '3': 2, '4': 1, '5': 5, '10': 'internalId'},
  ],
};

const ChooseResponse$json = const {
  '1': 'ChooseResponse',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
  ],
};

const SignInRequest$json = const {
  '1': 'SignInRequest',
  '2': const [
    const {'1': 'tt_session_id', '3': 1, '4': 1, '5': 9, '10': 'ttSessionId'},
    const {'1': 'pin', '3': 2, '4': 1, '5': 9, '10': 'pin'},
  ],
};

const SignInRetryRequest$json = const {
  '1': 'SignInRetryRequest',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
    const {'1': 'pin', '3': 2, '4': 1, '5': 9, '10': 'pin'},
    const {'1': 'captcha_text', '3': 3, '4': 1, '5': 9, '10': 'captchaText'},
    const {'1': 'selected_opt', '3': 4, '4': 1, '5': 9, '10': 'selectedOpt'},
  ],
};

const SignInEvent$json = const {
  '1': 'SignInEvent',
  '2': const [
    const {'1': 'type', '3': 1, '4': 1, '5': 14, '6': '.timeterm_cup.SignInEventType', '10': 'type'},
    const {'1': 'session', '3': 2, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
    const {'1': 'selected_opt', '3': 3, '4': 1, '5': 9, '10': 'selectedOpt'},
  ],
};

const TimetableRequest$json = const {
  '1': 'TimetableRequest',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
  ],
};

const TimetableWeek$json = const {
  '1': 'TimetableWeek',
  '2': const [
    const {'1': 'week', '3': 1, '4': 1, '5': 5, '10': 'week'},
    const {'1': 'appointments', '3': 2, '4': 3, '5': 11, '6': '.timeterm_cup.HistoryOption', '10': 'appointments'},
  ],
};

const Option$json = const {
  '1': 'Option',
  '2': const [
    const {'1': 'info', '3': 1, '4': 1, '5': 9, '10': 'info'},
    const {'1': 'text', '3': 2, '4': 1, '5': 9, '10': 'text'},
    const {'1': 'available_places', '3': 3, '4': 1, '5': 5, '10': 'availablePlaces'},
  ],
};

const HistoryOption$json = const {
  '1': 'HistoryOption',
  '2': const [
    const {'1': 'option', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Option', '10': 'option'},
    const {'1': 'date', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'date'},
    const {'1': 'slot', '3': 3, '4': 1, '5': 5, '10': 'slot'},
  ],
};

const GetUserRequest$json = const {
  '1': 'GetUserRequest',
  '2': const [
    const {'1': 'tt_session_id', '3': 1, '4': 1, '5': 9, '10': 'ttSessionId'},
  ],
};

const GetUserResponse$json = const {
  '1': 'GetUserResponse',
  '2': const [
    const {'1': 'cup_name', '3': 1, '4': 1, '5': 9, '10': 'cupName'},
  ],
};

const SetUserRequest$json = const {
  '1': 'SetUserRequest',
  '2': const [
    const {'1': 'tt_session_id', '3': 1, '4': 1, '5': 9, '10': 'ttSessionId'},
    const {'1': 'cup_name', '3': 2, '4': 1, '5': 9, '10': 'cupName'},
  ],
};

const PendingAppointmentsRequest$json = const {
  '1': 'PendingAppointmentsRequest',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
  ],
};

const PendingAppointmentsResponse$json = const {
  '1': 'PendingAppointmentsResponse',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
    const {'1': 'appointments', '3': 2, '4': 3, '5': 11, '6': '.timeterm_cup.Appointment', '10': 'appointments'},
  ],
};

const TimetableResponse$json = const {
  '1': 'TimetableResponse',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Session', '10': 'session'},
    const {'1': 'weeks', '3': 2, '4': 3, '5': 11, '6': '.timeterm_cup.TimetableWeek', '10': 'weeks'},
  ],
};

const Session$json = const {
  '1': 'Session',
  '2': const [
    const {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'asp_fields', '3': 2, '4': 3, '5': 11, '6': '.timeterm_cup.Session.AspFieldsEntry', '10': 'aspFields'},
    const {'1': 'extra_fields', '3': 3, '4': 3, '5': 11, '6': '.timeterm_cup.Session.ExtraFieldsEntry', '10': 'extraFields'},
    const {'1': 'last_request', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastRequest'},
  ],
  '3': const [Session_AspFieldsEntry$json, Session_ExtraFieldsEntry$json],
};

const Session_AspFieldsEntry$json = const {
  '1': 'AspFieldsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

const Session_ExtraFieldsEntry$json = const {
  '1': 'ExtraFieldsEntry',
  '2': const [
    const {'1': 'key', '3': 1, '4': 1, '5': 9, '10': 'key'},
    const {'1': 'value', '3': 2, '4': 1, '5': 9, '10': 'value'},
  ],
  '7': const {'7': true},
};

const Choice$json = const {
  '1': 'Choice',
  '2': const [
    const {'1': 'option', '3': 1, '4': 1, '5': 11, '6': '.timeterm_cup.Option', '10': 'option'},
    const {'1': 'full', '3': 2, '4': 1, '5': 8, '10': 'full'},
    const {'1': 'internal_id', '3': 3, '4': 1, '5': 5, '10': 'internalId'},
  ],
};

const Appointment$json = const {
  '1': 'Appointment',
  '2': const [
    const {'1': 'fixed', '3': 1, '4': 1, '5': 8, '10': 'fixed'},
    const {'1': 'slot', '3': 2, '4': 1, '5': 5, '10': 'slot'},
    const {'1': 'start_date', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'startDate'},
    const {'1': 'end_date', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'endDate'},
    const {'1': 'selected', '3': 5, '4': 1, '5': 11, '6': '.timeterm_cup.Option', '10': 'selected'},
    const {'1': 'choices', '3': 6, '4': 3, '5': 11, '6': '.timeterm_cup.Choice', '10': 'choices'},
  ],
};


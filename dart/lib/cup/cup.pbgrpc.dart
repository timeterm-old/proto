///
//  Generated code. Do not modify.
//  source: cup.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'cup.pb.dart' as $0;
import '../google/protobuf/empty.pb.dart' as $1;
export 'cup.pb.dart';

class CUPClient extends $grpc.Client {
  static final _$getUser =
      $grpc.ClientMethod<$0.GetUserRequest, $0.GetUserResponse>(
          '/timeterm_cup.CUP/GetUser',
          ($0.GetUserRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GetUserResponse.fromBuffer(value));
  static final _$setUser = $grpc.ClientMethod<$0.SetUserRequest, $1.Empty>(
      '/timeterm_cup.CUP/SetUser',
      ($0.SetUserRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));
  static final _$signIn = $grpc.ClientMethod<$0.SignInRequest, $0.SignInEvent>(
      '/timeterm_cup.CUP/SignIn',
      ($0.SignInRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.SignInEvent.fromBuffer(value));
  static final _$signInRetry =
      $grpc.ClientMethod<$0.SignInRetryRequest, $0.SignInEvent>(
          '/timeterm_cup.CUP/SignInRetry',
          ($0.SignInRetryRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.SignInEvent.fromBuffer(value));
  static final _$timetable =
      $grpc.ClientMethod<$0.TimetableRequest, $0.TimetableResponse>(
          '/timeterm_cup.CUP/Timetable',
          ($0.TimetableRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.TimetableResponse.fromBuffer(value));
  static final _$pendingAppointments = $grpc.ClientMethod<
          $0.PendingAppointmentsRequest, $0.PendingAppointmentsResponse>(
      '/timeterm_cup.CUP/PendingAppointments',
      ($0.PendingAppointmentsRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.PendingAppointmentsResponse.fromBuffer(value));
  static final _$choose =
      $grpc.ClientMethod<$0.ChooseRequest, $0.ChooseResponse>(
          '/timeterm_cup.CUP/Choose',
          ($0.ChooseRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.ChooseResponse.fromBuffer(value));
  static final _$searchUsers =
      $grpc.ClientMethod<$0.SearchUsersRequest, $0.SearchUsersResponse>(
          '/timeterm_cup.CUP/SearchUsers',
          ($0.SearchUsersRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SearchUsersResponse.fromBuffer(value));

  CUPClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.GetUserResponse> getUser($0.GetUserRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$getUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$1.Empty> setUser($0.SetUserRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$setUser, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.SignInEvent> signIn($0.SignInRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$signIn, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseStream<$0.SignInEvent> signInRetry(
      $0.SignInRetryRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$signInRetry, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.TimetableResponse> timetable(
      $0.TimetableRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$timetable, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.PendingAppointmentsResponse> pendingAppointments(
      $0.PendingAppointmentsRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$pendingAppointments, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ChooseResponse> choose($0.ChooseRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$choose, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SearchUsersResponse> searchUsers(
      $0.SearchUsersRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$searchUsers, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class CUPServiceBase extends $grpc.Service {
  $core.String get $name => 'timeterm_cup.CUP';

  CUPServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.GetUserRequest, $0.GetUserResponse>(
        'GetUser',
        getUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GetUserRequest.fromBuffer(value),
        ($0.GetUserResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SetUserRequest, $1.Empty>(
        'SetUser',
        setUser_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SetUserRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SignInRequest, $0.SignInEvent>(
        'SignIn',
        signIn_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.SignInRequest.fromBuffer(value),
        ($0.SignInEvent value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SignInRetryRequest, $0.SignInEvent>(
        'SignInRetry',
        signInRetry_Pre,
        false,
        true,
        ($core.List<$core.int> value) =>
            $0.SignInRetryRequest.fromBuffer(value),
        ($0.SignInEvent value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.TimetableRequest, $0.TimetableResponse>(
        'Timetable',
        timetable_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.TimetableRequest.fromBuffer(value),
        ($0.TimetableResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.PendingAppointmentsRequest,
            $0.PendingAppointmentsResponse>(
        'PendingAppointments',
        pendingAppointments_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.PendingAppointmentsRequest.fromBuffer(value),
        ($0.PendingAppointmentsResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ChooseRequest, $0.ChooseResponse>(
        'Choose',
        choose_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ChooseRequest.fromBuffer(value),
        ($0.ChooseResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.SearchUsersRequest, $0.SearchUsersResponse>(
            'SearchUsers',
            searchUsers_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.SearchUsersRequest.fromBuffer(value),
            ($0.SearchUsersResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.GetUserResponse> getUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.GetUserRequest> request) async {
    return getUser(call, await request);
  }

  $async.Future<$1.Empty> setUser_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SetUserRequest> request) async {
    return setUser(call, await request);
  }

  $async.Stream<$0.SignInEvent> signIn_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SignInRequest> request) async* {
    yield* signIn(call, await request);
  }

  $async.Stream<$0.SignInEvent> signInRetry_Pre($grpc.ServiceCall call,
      $async.Future<$0.SignInRetryRequest> request) async* {
    yield* signInRetry(call, await request);
  }

  $async.Future<$0.TimetableResponse> timetable_Pre($grpc.ServiceCall call,
      $async.Future<$0.TimetableRequest> request) async {
    return timetable(call, await request);
  }

  $async.Future<$0.PendingAppointmentsResponse> pendingAppointments_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.PendingAppointmentsRequest> request) async {
    return pendingAppointments(call, await request);
  }

  $async.Future<$0.ChooseResponse> choose_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ChooseRequest> request) async {
    return choose(call, await request);
  }

  $async.Future<$0.SearchUsersResponse> searchUsers_Pre($grpc.ServiceCall call,
      $async.Future<$0.SearchUsersRequest> request) async {
    return searchUsers(call, await request);
  }

  $async.Future<$0.GetUserResponse> getUser(
      $grpc.ServiceCall call, $0.GetUserRequest request);
  $async.Future<$1.Empty> setUser(
      $grpc.ServiceCall call, $0.SetUserRequest request);
  $async.Stream<$0.SignInEvent> signIn(
      $grpc.ServiceCall call, $0.SignInRequest request);
  $async.Stream<$0.SignInEvent> signInRetry(
      $grpc.ServiceCall call, $0.SignInRetryRequest request);
  $async.Future<$0.TimetableResponse> timetable(
      $grpc.ServiceCall call, $0.TimetableRequest request);
  $async.Future<$0.PendingAppointmentsResponse> pendingAppointments(
      $grpc.ServiceCall call, $0.PendingAppointmentsRequest request);
  $async.Future<$0.ChooseResponse> choose(
      $grpc.ServiceCall call, $0.ChooseRequest request);
  $async.Future<$0.SearchUsersResponse> searchUsers(
      $grpc.ServiceCall call, $0.SearchUsersRequest request);
}

///
//  Generated code. Do not modify.
//  source: cup.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class SignInEventType extends $pb.ProtobufEnum {
  static const SignInEventType SE_SEARCHING_NAME = SignInEventType._(0, 'SE_SEARCHING_NAME');
  static const SignInEventType SE_SELECTING_USER = SignInEventType._(1, 'SE_SELECTING_USER');
  static const SignInEventType SE_SIGNING_IN = SignInEventType._(2, 'SE_SIGNING_IN');
  static const SignInEventType SE_SIGNED_IN = SignInEventType._(3, 'SE_SIGNED_IN');
  static const SignInEventType SE_FAILED = SignInEventType._(4, 'SE_FAILED');

  static const $core.List<SignInEventType> values = <SignInEventType> [
    SE_SEARCHING_NAME,
    SE_SELECTING_USER,
    SE_SIGNING_IN,
    SE_SIGNED_IN,
    SE_FAILED,
  ];

  static final $core.Map<$core.int, SignInEventType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static SignInEventType valueOf($core.int value) => _byValue[value];

  const SignInEventType._($core.int v, $core.String n) : super(v, n);
}


///
//  Generated code. Do not modify.
//  source: cup.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../google/protobuf/timestamp.pb.dart' as $2;

import 'cup.pbenum.dart';

export 'cup.pbenum.dart';

class SearchUsersRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SearchUsersRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'firstLetters')
    ..hasRequiredFields = false
  ;

  SearchUsersRequest._() : super();
  factory SearchUsersRequest() => create();
  factory SearchUsersRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchUsersRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SearchUsersRequest clone() => SearchUsersRequest()..mergeFromMessage(this);
  SearchUsersRequest copyWith(void Function(SearchUsersRequest) updates) => super.copyWith((message) => updates(message as SearchUsersRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchUsersRequest create() => SearchUsersRequest._();
  SearchUsersRequest createEmptyInstance() => create();
  static $pb.PbList<SearchUsersRequest> createRepeated() => $pb.PbList<SearchUsersRequest>();
  @$core.pragma('dart2js:noInline')
  static SearchUsersRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchUsersRequest>(create);
  static SearchUsersRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get firstLetters => $_getSZ(0);
  @$pb.TagNumber(1)
  set firstLetters($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFirstLetters() => $_has(0);
  @$pb.TagNumber(1)
  void clearFirstLetters() => clearField(1);
}

class SearchUsersResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SearchUsersResponse', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..pPS(1, 'users')
    ..hasRequiredFields = false
  ;

  SearchUsersResponse._() : super();
  factory SearchUsersResponse() => create();
  factory SearchUsersResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SearchUsersResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SearchUsersResponse clone() => SearchUsersResponse()..mergeFromMessage(this);
  SearchUsersResponse copyWith(void Function(SearchUsersResponse) updates) => super.copyWith((message) => updates(message as SearchUsersResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SearchUsersResponse create() => SearchUsersResponse._();
  SearchUsersResponse createEmptyInstance() => create();
  static $pb.PbList<SearchUsersResponse> createRepeated() => $pb.PbList<SearchUsersResponse>();
  @$core.pragma('dart2js:noInline')
  static SearchUsersResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SearchUsersResponse>(create);
  static SearchUsersResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get users => $_getList(0);
}

class ChooseRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ChooseRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..a<$core.int>(2, 'internalId', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  ChooseRequest._() : super();
  factory ChooseRequest() => create();
  factory ChooseRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ChooseRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ChooseRequest clone() => ChooseRequest()..mergeFromMessage(this);
  ChooseRequest copyWith(void Function(ChooseRequest) updates) => super.copyWith((message) => updates(message as ChooseRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ChooseRequest create() => ChooseRequest._();
  ChooseRequest createEmptyInstance() => create();
  static $pb.PbList<ChooseRequest> createRepeated() => $pb.PbList<ChooseRequest>();
  @$core.pragma('dart2js:noInline')
  static ChooseRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ChooseRequest>(create);
  static ChooseRequest _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.int get internalId => $_getIZ(1);
  @$pb.TagNumber(2)
  set internalId($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasInternalId() => $_has(1);
  @$pb.TagNumber(2)
  void clearInternalId() => clearField(2);
}

class ChooseResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ChooseResponse', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..hasRequiredFields = false
  ;

  ChooseResponse._() : super();
  factory ChooseResponse() => create();
  factory ChooseResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ChooseResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ChooseResponse clone() => ChooseResponse()..mergeFromMessage(this);
  ChooseResponse copyWith(void Function(ChooseResponse) updates) => super.copyWith((message) => updates(message as ChooseResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ChooseResponse create() => ChooseResponse._();
  ChooseResponse createEmptyInstance() => create();
  static $pb.PbList<ChooseResponse> createRepeated() => $pb.PbList<ChooseResponse>();
  @$core.pragma('dart2js:noInline')
  static ChooseResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ChooseResponse>(create);
  static ChooseResponse _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);
}

class SignInRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SignInRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'ttSessionId')
    ..aOS(2, 'pin')
    ..hasRequiredFields = false
  ;

  SignInRequest._() : super();
  factory SignInRequest() => create();
  factory SignInRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SignInRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SignInRequest clone() => SignInRequest()..mergeFromMessage(this);
  SignInRequest copyWith(void Function(SignInRequest) updates) => super.copyWith((message) => updates(message as SignInRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SignInRequest create() => SignInRequest._();
  SignInRequest createEmptyInstance() => create();
  static $pb.PbList<SignInRequest> createRepeated() => $pb.PbList<SignInRequest>();
  @$core.pragma('dart2js:noInline')
  static SignInRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SignInRequest>(create);
  static SignInRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get ttSessionId => $_getSZ(0);
  @$pb.TagNumber(1)
  set ttSessionId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTtSessionId() => $_has(0);
  @$pb.TagNumber(1)
  void clearTtSessionId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get pin => $_getSZ(1);
  @$pb.TagNumber(2)
  set pin($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPin() => $_has(1);
  @$pb.TagNumber(2)
  void clearPin() => clearField(2);
}

class SignInRetryRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SignInRetryRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..aOS(2, 'pin')
    ..aOS(3, 'captchaText')
    ..aOS(4, 'selectedOpt')
    ..hasRequiredFields = false
  ;

  SignInRetryRequest._() : super();
  factory SignInRetryRequest() => create();
  factory SignInRetryRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SignInRetryRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SignInRetryRequest clone() => SignInRetryRequest()..mergeFromMessage(this);
  SignInRetryRequest copyWith(void Function(SignInRetryRequest) updates) => super.copyWith((message) => updates(message as SignInRetryRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SignInRetryRequest create() => SignInRetryRequest._();
  SignInRetryRequest createEmptyInstance() => create();
  static $pb.PbList<SignInRetryRequest> createRepeated() => $pb.PbList<SignInRetryRequest>();
  @$core.pragma('dart2js:noInline')
  static SignInRetryRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SignInRetryRequest>(create);
  static SignInRetryRequest _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.String get pin => $_getSZ(1);
  @$pb.TagNumber(2)
  set pin($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPin() => $_has(1);
  @$pb.TagNumber(2)
  void clearPin() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get captchaText => $_getSZ(2);
  @$pb.TagNumber(3)
  set captchaText($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasCaptchaText() => $_has(2);
  @$pb.TagNumber(3)
  void clearCaptchaText() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get selectedOpt => $_getSZ(3);
  @$pb.TagNumber(4)
  set selectedOpt($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasSelectedOpt() => $_has(3);
  @$pb.TagNumber(4)
  void clearSelectedOpt() => clearField(4);
}

class SignInEvent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SignInEvent', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..e<SignInEventType>(1, 'type', $pb.PbFieldType.OE, defaultOrMaker: SignInEventType.SE_SEARCHING_NAME, valueOf: SignInEventType.valueOf, enumValues: SignInEventType.values)
    ..aOM<Session>(2, 'session', subBuilder: Session.create)
    ..aOS(3, 'selectedOpt')
    ..hasRequiredFields = false
  ;

  SignInEvent._() : super();
  factory SignInEvent() => create();
  factory SignInEvent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SignInEvent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SignInEvent clone() => SignInEvent()..mergeFromMessage(this);
  SignInEvent copyWith(void Function(SignInEvent) updates) => super.copyWith((message) => updates(message as SignInEvent));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SignInEvent create() => SignInEvent._();
  SignInEvent createEmptyInstance() => create();
  static $pb.PbList<SignInEvent> createRepeated() => $pb.PbList<SignInEvent>();
  @$core.pragma('dart2js:noInline')
  static SignInEvent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SignInEvent>(create);
  static SignInEvent _defaultInstance;

  @$pb.TagNumber(1)
  SignInEventType get type => $_getN(0);
  @$pb.TagNumber(1)
  set type(SignInEventType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasType() => $_has(0);
  @$pb.TagNumber(1)
  void clearType() => clearField(1);

  @$pb.TagNumber(2)
  Session get session => $_getN(1);
  @$pb.TagNumber(2)
  set session(Session v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasSession() => $_has(1);
  @$pb.TagNumber(2)
  void clearSession() => clearField(2);
  @$pb.TagNumber(2)
  Session ensureSession() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get selectedOpt => $_getSZ(2);
  @$pb.TagNumber(3)
  set selectedOpt($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSelectedOpt() => $_has(2);
  @$pb.TagNumber(3)
  void clearSelectedOpt() => clearField(3);
}

class TimetableRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TimetableRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..hasRequiredFields = false
  ;

  TimetableRequest._() : super();
  factory TimetableRequest() => create();
  factory TimetableRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TimetableRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TimetableRequest clone() => TimetableRequest()..mergeFromMessage(this);
  TimetableRequest copyWith(void Function(TimetableRequest) updates) => super.copyWith((message) => updates(message as TimetableRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TimetableRequest create() => TimetableRequest._();
  TimetableRequest createEmptyInstance() => create();
  static $pb.PbList<TimetableRequest> createRepeated() => $pb.PbList<TimetableRequest>();
  @$core.pragma('dart2js:noInline')
  static TimetableRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TimetableRequest>(create);
  static TimetableRequest _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);
}

class TimetableWeek extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TimetableWeek', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..a<$core.int>(1, 'week', $pb.PbFieldType.O3)
    ..pc<HistoryOption>(2, 'appointments', $pb.PbFieldType.PM, subBuilder: HistoryOption.create)
    ..hasRequiredFields = false
  ;

  TimetableWeek._() : super();
  factory TimetableWeek() => create();
  factory TimetableWeek.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TimetableWeek.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TimetableWeek clone() => TimetableWeek()..mergeFromMessage(this);
  TimetableWeek copyWith(void Function(TimetableWeek) updates) => super.copyWith((message) => updates(message as TimetableWeek));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TimetableWeek create() => TimetableWeek._();
  TimetableWeek createEmptyInstance() => create();
  static $pb.PbList<TimetableWeek> createRepeated() => $pb.PbList<TimetableWeek>();
  @$core.pragma('dart2js:noInline')
  static TimetableWeek getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TimetableWeek>(create);
  static TimetableWeek _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get week => $_getIZ(0);
  @$pb.TagNumber(1)
  set week($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasWeek() => $_has(0);
  @$pb.TagNumber(1)
  void clearWeek() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<HistoryOption> get appointments => $_getList(1);
}

class Option extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Option', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'info')
    ..aOS(2, 'text')
    ..a<$core.int>(3, 'availablePlaces', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Option._() : super();
  factory Option() => create();
  factory Option.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Option.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Option clone() => Option()..mergeFromMessage(this);
  Option copyWith(void Function(Option) updates) => super.copyWith((message) => updates(message as Option));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Option create() => Option._();
  Option createEmptyInstance() => create();
  static $pb.PbList<Option> createRepeated() => $pb.PbList<Option>();
  @$core.pragma('dart2js:noInline')
  static Option getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Option>(create);
  static Option _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get info => $_getSZ(0);
  @$pb.TagNumber(1)
  set info($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasInfo() => $_has(0);
  @$pb.TagNumber(1)
  void clearInfo() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get text => $_getSZ(1);
  @$pb.TagNumber(2)
  set text($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasText() => $_has(1);
  @$pb.TagNumber(2)
  void clearText() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get availablePlaces => $_getIZ(2);
  @$pb.TagNumber(3)
  set availablePlaces($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAvailablePlaces() => $_has(2);
  @$pb.TagNumber(3)
  void clearAvailablePlaces() => clearField(3);
}

class HistoryOption extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('HistoryOption', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Option>(1, 'option', subBuilder: Option.create)
    ..aOM<$2.Timestamp>(2, 'date', subBuilder: $2.Timestamp.create)
    ..a<$core.int>(3, 'slot', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  HistoryOption._() : super();
  factory HistoryOption() => create();
  factory HistoryOption.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory HistoryOption.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  HistoryOption clone() => HistoryOption()..mergeFromMessage(this);
  HistoryOption copyWith(void Function(HistoryOption) updates) => super.copyWith((message) => updates(message as HistoryOption));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static HistoryOption create() => HistoryOption._();
  HistoryOption createEmptyInstance() => create();
  static $pb.PbList<HistoryOption> createRepeated() => $pb.PbList<HistoryOption>();
  @$core.pragma('dart2js:noInline')
  static HistoryOption getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<HistoryOption>(create);
  static HistoryOption _defaultInstance;

  @$pb.TagNumber(1)
  Option get option => $_getN(0);
  @$pb.TagNumber(1)
  set option(Option v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasOption() => $_has(0);
  @$pb.TagNumber(1)
  void clearOption() => clearField(1);
  @$pb.TagNumber(1)
  Option ensureOption() => $_ensure(0);

  @$pb.TagNumber(2)
  $2.Timestamp get date => $_getN(1);
  @$pb.TagNumber(2)
  set date($2.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasDate() => $_has(1);
  @$pb.TagNumber(2)
  void clearDate() => clearField(2);
  @$pb.TagNumber(2)
  $2.Timestamp ensureDate() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.int get slot => $_getIZ(2);
  @$pb.TagNumber(3)
  set slot($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSlot() => $_has(2);
  @$pb.TagNumber(3)
  void clearSlot() => clearField(3);
}

class GetUserRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetUserRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'ttSessionId')
    ..hasRequiredFields = false
  ;

  GetUserRequest._() : super();
  factory GetUserRequest() => create();
  factory GetUserRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetUserRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetUserRequest clone() => GetUserRequest()..mergeFromMessage(this);
  GetUserRequest copyWith(void Function(GetUserRequest) updates) => super.copyWith((message) => updates(message as GetUserRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetUserRequest create() => GetUserRequest._();
  GetUserRequest createEmptyInstance() => create();
  static $pb.PbList<GetUserRequest> createRepeated() => $pb.PbList<GetUserRequest>();
  @$core.pragma('dart2js:noInline')
  static GetUserRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetUserRequest>(create);
  static GetUserRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get ttSessionId => $_getSZ(0);
  @$pb.TagNumber(1)
  set ttSessionId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTtSessionId() => $_has(0);
  @$pb.TagNumber(1)
  void clearTtSessionId() => clearField(1);
}

class GetUserResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetUserResponse', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'cupName')
    ..hasRequiredFields = false
  ;

  GetUserResponse._() : super();
  factory GetUserResponse() => create();
  factory GetUserResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetUserResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetUserResponse clone() => GetUserResponse()..mergeFromMessage(this);
  GetUserResponse copyWith(void Function(GetUserResponse) updates) => super.copyWith((message) => updates(message as GetUserResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetUserResponse create() => GetUserResponse._();
  GetUserResponse createEmptyInstance() => create();
  static $pb.PbList<GetUserResponse> createRepeated() => $pb.PbList<GetUserResponse>();
  @$core.pragma('dart2js:noInline')
  static GetUserResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetUserResponse>(create);
  static GetUserResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get cupName => $_getSZ(0);
  @$pb.TagNumber(1)
  set cupName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCupName() => $_has(0);
  @$pb.TagNumber(1)
  void clearCupName() => clearField(1);
}

class SetUserRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SetUserRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'ttSessionId')
    ..aOS(2, 'cupName')
    ..hasRequiredFields = false
  ;

  SetUserRequest._() : super();
  factory SetUserRequest() => create();
  factory SetUserRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SetUserRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SetUserRequest clone() => SetUserRequest()..mergeFromMessage(this);
  SetUserRequest copyWith(void Function(SetUserRequest) updates) => super.copyWith((message) => updates(message as SetUserRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SetUserRequest create() => SetUserRequest._();
  SetUserRequest createEmptyInstance() => create();
  static $pb.PbList<SetUserRequest> createRepeated() => $pb.PbList<SetUserRequest>();
  @$core.pragma('dart2js:noInline')
  static SetUserRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SetUserRequest>(create);
  static SetUserRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get ttSessionId => $_getSZ(0);
  @$pb.TagNumber(1)
  set ttSessionId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasTtSessionId() => $_has(0);
  @$pb.TagNumber(1)
  void clearTtSessionId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get cupName => $_getSZ(1);
  @$pb.TagNumber(2)
  set cupName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCupName() => $_has(1);
  @$pb.TagNumber(2)
  void clearCupName() => clearField(2);
}

class PendingAppointmentsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PendingAppointmentsRequest', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..hasRequiredFields = false
  ;

  PendingAppointmentsRequest._() : super();
  factory PendingAppointmentsRequest() => create();
  factory PendingAppointmentsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PendingAppointmentsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  PendingAppointmentsRequest clone() => PendingAppointmentsRequest()..mergeFromMessage(this);
  PendingAppointmentsRequest copyWith(void Function(PendingAppointmentsRequest) updates) => super.copyWith((message) => updates(message as PendingAppointmentsRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PendingAppointmentsRequest create() => PendingAppointmentsRequest._();
  PendingAppointmentsRequest createEmptyInstance() => create();
  static $pb.PbList<PendingAppointmentsRequest> createRepeated() => $pb.PbList<PendingAppointmentsRequest>();
  @$core.pragma('dart2js:noInline')
  static PendingAppointmentsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PendingAppointmentsRequest>(create);
  static PendingAppointmentsRequest _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);
}

class PendingAppointmentsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('PendingAppointmentsResponse', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..pc<Appointment>(2, 'appointments', $pb.PbFieldType.PM, subBuilder: Appointment.create)
    ..hasRequiredFields = false
  ;

  PendingAppointmentsResponse._() : super();
  factory PendingAppointmentsResponse() => create();
  factory PendingAppointmentsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PendingAppointmentsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  PendingAppointmentsResponse clone() => PendingAppointmentsResponse()..mergeFromMessage(this);
  PendingAppointmentsResponse copyWith(void Function(PendingAppointmentsResponse) updates) => super.copyWith((message) => updates(message as PendingAppointmentsResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static PendingAppointmentsResponse create() => PendingAppointmentsResponse._();
  PendingAppointmentsResponse createEmptyInstance() => create();
  static $pb.PbList<PendingAppointmentsResponse> createRepeated() => $pb.PbList<PendingAppointmentsResponse>();
  @$core.pragma('dart2js:noInline')
  static PendingAppointmentsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PendingAppointmentsResponse>(create);
  static PendingAppointmentsResponse _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<Appointment> get appointments => $_getList(1);
}

class TimetableResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('TimetableResponse', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..pc<TimetableWeek>(2, 'weeks', $pb.PbFieldType.PM, subBuilder: TimetableWeek.create)
    ..hasRequiredFields = false
  ;

  TimetableResponse._() : super();
  factory TimetableResponse() => create();
  factory TimetableResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory TimetableResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  TimetableResponse clone() => TimetableResponse()..mergeFromMessage(this);
  TimetableResponse copyWith(void Function(TimetableResponse) updates) => super.copyWith((message) => updates(message as TimetableResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static TimetableResponse create() => TimetableResponse._();
  TimetableResponse createEmptyInstance() => create();
  static $pb.PbList<TimetableResponse> createRepeated() => $pb.PbList<TimetableResponse>();
  @$core.pragma('dart2js:noInline')
  static TimetableResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<TimetableResponse>(create);
  static TimetableResponse _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.List<TimetableWeek> get weeks => $_getList(1);
}

class Session extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Session', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOS(1, 'token')
    ..m<$core.String, $core.String>(2, 'aspFields', entryClassName: 'Session.AspFieldsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('timeterm_cup'))
    ..m<$core.String, $core.String>(3, 'extraFields', entryClassName: 'Session.ExtraFieldsEntry', keyFieldType: $pb.PbFieldType.OS, valueFieldType: $pb.PbFieldType.OS, packageName: const $pb.PackageName('timeterm_cup'))
    ..aOM<$2.Timestamp>(4, 'lastRequest', subBuilder: $2.Timestamp.create)
    ..hasRequiredFields = false
  ;

  Session._() : super();
  factory Session() => create();
  factory Session.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Session.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Session clone() => Session()..mergeFromMessage(this);
  Session copyWith(void Function(Session) updates) => super.copyWith((message) => updates(message as Session));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Session create() => Session._();
  Session createEmptyInstance() => create();
  static $pb.PbList<Session> createRepeated() => $pb.PbList<Session>();
  @$core.pragma('dart2js:noInline')
  static Session getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Session>(create);
  static Session _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $core.Map<$core.String, $core.String> get aspFields => $_getMap(1);

  @$pb.TagNumber(3)
  $core.Map<$core.String, $core.String> get extraFields => $_getMap(2);

  @$pb.TagNumber(4)
  $2.Timestamp get lastRequest => $_getN(3);
  @$pb.TagNumber(4)
  set lastRequest($2.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasLastRequest() => $_has(3);
  @$pb.TagNumber(4)
  void clearLastRequest() => clearField(4);
  @$pb.TagNumber(4)
  $2.Timestamp ensureLastRequest() => $_ensure(3);
}

class Choice extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Choice', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOM<Option>(1, 'option', subBuilder: Option.create)
    ..aOB(2, 'full')
    ..a<$core.int>(3, 'internalId', $pb.PbFieldType.O3)
    ..hasRequiredFields = false
  ;

  Choice._() : super();
  factory Choice() => create();
  factory Choice.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Choice.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Choice clone() => Choice()..mergeFromMessage(this);
  Choice copyWith(void Function(Choice) updates) => super.copyWith((message) => updates(message as Choice));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Choice create() => Choice._();
  Choice createEmptyInstance() => create();
  static $pb.PbList<Choice> createRepeated() => $pb.PbList<Choice>();
  @$core.pragma('dart2js:noInline')
  static Choice getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Choice>(create);
  static Choice _defaultInstance;

  @$pb.TagNumber(1)
  Option get option => $_getN(0);
  @$pb.TagNumber(1)
  set option(Option v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasOption() => $_has(0);
  @$pb.TagNumber(1)
  void clearOption() => clearField(1);
  @$pb.TagNumber(1)
  Option ensureOption() => $_ensure(0);

  @$pb.TagNumber(2)
  $core.bool get full => $_getBF(1);
  @$pb.TagNumber(2)
  set full($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasFull() => $_has(1);
  @$pb.TagNumber(2)
  void clearFull() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get internalId => $_getIZ(2);
  @$pb.TagNumber(3)
  set internalId($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasInternalId() => $_has(2);
  @$pb.TagNumber(3)
  void clearInternalId() => clearField(3);
}

class Appointment extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Appointment', package: const $pb.PackageName('timeterm_cup'), createEmptyInstance: create)
    ..aOB(1, 'fixed')
    ..a<$core.int>(2, 'slot', $pb.PbFieldType.O3)
    ..aOM<$2.Timestamp>(3, 'startDate', subBuilder: $2.Timestamp.create)
    ..aOM<$2.Timestamp>(4, 'endDate', subBuilder: $2.Timestamp.create)
    ..aOM<Option>(5, 'selected', subBuilder: Option.create)
    ..pc<Choice>(6, 'choices', $pb.PbFieldType.PM, subBuilder: Choice.create)
    ..hasRequiredFields = false
  ;

  Appointment._() : super();
  factory Appointment() => create();
  factory Appointment.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Appointment.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Appointment clone() => Appointment()..mergeFromMessage(this);
  Appointment copyWith(void Function(Appointment) updates) => super.copyWith((message) => updates(message as Appointment));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Appointment create() => Appointment._();
  Appointment createEmptyInstance() => create();
  static $pb.PbList<Appointment> createRepeated() => $pb.PbList<Appointment>();
  @$core.pragma('dart2js:noInline')
  static Appointment getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Appointment>(create);
  static Appointment _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get fixed => $_getBF(0);
  @$pb.TagNumber(1)
  set fixed($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFixed() => $_has(0);
  @$pb.TagNumber(1)
  void clearFixed() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get slot => $_getIZ(1);
  @$pb.TagNumber(2)
  set slot($core.int v) { $_setSignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSlot() => $_has(1);
  @$pb.TagNumber(2)
  void clearSlot() => clearField(2);

  @$pb.TagNumber(3)
  $2.Timestamp get startDate => $_getN(2);
  @$pb.TagNumber(3)
  set startDate($2.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStartDate() => $_has(2);
  @$pb.TagNumber(3)
  void clearStartDate() => clearField(3);
  @$pb.TagNumber(3)
  $2.Timestamp ensureStartDate() => $_ensure(2);

  @$pb.TagNumber(4)
  $2.Timestamp get endDate => $_getN(3);
  @$pb.TagNumber(4)
  set endDate($2.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasEndDate() => $_has(3);
  @$pb.TagNumber(4)
  void clearEndDate() => clearField(4);
  @$pb.TagNumber(4)
  $2.Timestamp ensureEndDate() => $_ensure(3);

  @$pb.TagNumber(5)
  Option get selected => $_getN(4);
  @$pb.TagNumber(5)
  set selected(Option v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasSelected() => $_has(4);
  @$pb.TagNumber(5)
  void clearSelected() => clearField(5);
  @$pb.TagNumber(5)
  Option ensureSelected() => $_ensure(4);

  @$pb.TagNumber(6)
  $core.List<Choice> get choices => $_getList(5);
}


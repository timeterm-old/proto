///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'user.pb.dart' as $0;
import '../google/protobuf/empty.pb.dart' as $1;
export 'user.pb.dart';

class UserClient extends $grpc.Client {
  static final _$newSession =
      $grpc.ClientMethod<$0.SessionRequest, $0.SessionResponse>(
          '/timeterm_user.User/NewSession',
          ($0.SessionRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SessionResponse.fromBuffer(value));
  static final _$validateSession =
      $grpc.ClientMethod<$0.ValidateSessionRequest, $0.ValidateSessionResponse>(
          '/timeterm_user.User/ValidateSession',
          ($0.ValidateSessionRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ValidateSessionResponse.fromBuffer(value));
  static final _$getUserCode =
      $grpc.ClientMethod<$0.UserCodeRequest, $0.UserCodeResponse>(
          '/timeterm_user.User/GetUserCode',
          ($0.UserCodeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.UserCodeResponse.fromBuffer(value));
  static final _$createUserCode =
      $grpc.ClientMethod<$0.CreateUserCodeRequest, $1.Empty>(
          '/timeterm_user.User/CreateUserCode',
          ($0.CreateUserCodeRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.Empty.fromBuffer(value));

  UserClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.SessionResponse> newSession($0.SessionRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$newSession, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ValidateSessionResponse> validateSession(
      $0.ValidateSessionRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$validateSession, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.UserCodeResponse> getUserCode(
      $0.UserCodeRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getUserCode, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$1.Empty> createUserCode(
      $0.CreateUserCodeRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$createUserCode, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class UserServiceBase extends $grpc.Service {
  $core.String get $name => 'timeterm_user.User';

  UserServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.SessionRequest, $0.SessionResponse>(
        'NewSession',
        newSession_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SessionRequest.fromBuffer(value),
        ($0.SessionResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ValidateSessionRequest,
            $0.ValidateSessionResponse>(
        'ValidateSession',
        validateSession_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ValidateSessionRequest.fromBuffer(value),
        ($0.ValidateSessionResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.UserCodeRequest, $0.UserCodeResponse>(
        'GetUserCode',
        getUserCode_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.UserCodeRequest.fromBuffer(value),
        ($0.UserCodeResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.CreateUserCodeRequest, $1.Empty>(
        'CreateUserCode',
        createUserCode_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.CreateUserCodeRequest.fromBuffer(value),
        ($1.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$0.SessionResponse> newSession_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SessionRequest> request) async {
    return newSession(call, await request);
  }

  $async.Future<$0.ValidateSessionResponse> validateSession_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ValidateSessionRequest> request) async {
    return validateSession(call, await request);
  }

  $async.Future<$0.UserCodeResponse> getUserCode_Pre(
      $grpc.ServiceCall call, $async.Future<$0.UserCodeRequest> request) async {
    return getUserCode(call, await request);
  }

  $async.Future<$1.Empty> createUserCode_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateUserCodeRequest> request) async {
    return createUserCode(call, await request);
  }

  $async.Future<$0.SessionResponse> newSession(
      $grpc.ServiceCall call, $0.SessionRequest request);
  $async.Future<$0.ValidateSessionResponse> validateSession(
      $grpc.ServiceCall call, $0.ValidateSessionRequest request);
  $async.Future<$0.UserCodeResponse> getUserCode(
      $grpc.ServiceCall call, $0.UserCodeRequest request);
  $async.Future<$1.Empty> createUserCode(
      $grpc.ServiceCall call, $0.CreateUserCodeRequest request);
}

///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import '../google/protobuf/duration.pb.dart' as $2;

class SessionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SessionRequest', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..a<$core.int>(1, 'cardId', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  SessionRequest._() : super();
  factory SessionRequest() => create();
  factory SessionRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SessionRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SessionRequest clone() => SessionRequest()..mergeFromMessage(this);
  SessionRequest copyWith(void Function(SessionRequest) updates) => super.copyWith((message) => updates(message as SessionRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SessionRequest create() => SessionRequest._();
  SessionRequest createEmptyInstance() => create();
  static $pb.PbList<SessionRequest> createRepeated() => $pb.PbList<SessionRequest>();
  @$core.pragma('dart2js:noInline')
  static SessionRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SessionRequest>(create);
  static SessionRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get cardId => $_getIZ(0);
  @$pb.TagNumber(1)
  set cardId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCardId() => $_has(0);
  @$pb.TagNumber(1)
  void clearCardId() => clearField(1);
}

class SessionResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SessionResponse', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..aOM<Session>(1, 'session', subBuilder: Session.create)
    ..hasRequiredFields = false
  ;

  SessionResponse._() : super();
  factory SessionResponse() => create();
  factory SessionResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SessionResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SessionResponse clone() => SessionResponse()..mergeFromMessage(this);
  SessionResponse copyWith(void Function(SessionResponse) updates) => super.copyWith((message) => updates(message as SessionResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SessionResponse create() => SessionResponse._();
  SessionResponse createEmptyInstance() => create();
  static $pb.PbList<SessionResponse> createRepeated() => $pb.PbList<SessionResponse>();
  @$core.pragma('dart2js:noInline')
  static SessionResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SessionResponse>(create);
  static SessionResponse _defaultInstance;

  @$pb.TagNumber(1)
  Session get session => $_getN(0);
  @$pb.TagNumber(1)
  set session(Session v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasSession() => $_has(0);
  @$pb.TagNumber(1)
  void clearSession() => clearField(1);
  @$pb.TagNumber(1)
  Session ensureSession() => $_ensure(0);
}

class ValidateSessionRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ValidateSessionRequest', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..aOS(1, 'sessionId')
    ..hasRequiredFields = false
  ;

  ValidateSessionRequest._() : super();
  factory ValidateSessionRequest() => create();
  factory ValidateSessionRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ValidateSessionRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ValidateSessionRequest clone() => ValidateSessionRequest()..mergeFromMessage(this);
  ValidateSessionRequest copyWith(void Function(ValidateSessionRequest) updates) => super.copyWith((message) => updates(message as ValidateSessionRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ValidateSessionRequest create() => ValidateSessionRequest._();
  ValidateSessionRequest createEmptyInstance() => create();
  static $pb.PbList<ValidateSessionRequest> createRepeated() => $pb.PbList<ValidateSessionRequest>();
  @$core.pragma('dart2js:noInline')
  static ValidateSessionRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ValidateSessionRequest>(create);
  static ValidateSessionRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get sessionId => $_getSZ(0);
  @$pb.TagNumber(1)
  set sessionId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSessionId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSessionId() => clearField(1);
}

class ValidateSessionResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ValidateSessionResponse', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..aOB(1, 'valid')
    ..aOM<Session>(2, 'session', subBuilder: Session.create)
    ..hasRequiredFields = false
  ;

  ValidateSessionResponse._() : super();
  factory ValidateSessionResponse() => create();
  factory ValidateSessionResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ValidateSessionResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ValidateSessionResponse clone() => ValidateSessionResponse()..mergeFromMessage(this);
  ValidateSessionResponse copyWith(void Function(ValidateSessionResponse) updates) => super.copyWith((message) => updates(message as ValidateSessionResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ValidateSessionResponse create() => ValidateSessionResponse._();
  ValidateSessionResponse createEmptyInstance() => create();
  static $pb.PbList<ValidateSessionResponse> createRepeated() => $pb.PbList<ValidateSessionResponse>();
  @$core.pragma('dart2js:noInline')
  static ValidateSessionResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ValidateSessionResponse>(create);
  static ValidateSessionResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get valid => $_getBF(0);
  @$pb.TagNumber(1)
  set valid($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasValid() => $_has(0);
  @$pb.TagNumber(1)
  void clearValid() => clearField(1);

  @$pb.TagNumber(2)
  Session get session => $_getN(1);
  @$pb.TagNumber(2)
  set session(Session v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasSession() => $_has(1);
  @$pb.TagNumber(2)
  void clearSession() => clearField(2);
  @$pb.TagNumber(2)
  Session ensureSession() => $_ensure(1);
}

class UserCodeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('UserCodeRequest', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..a<$core.int>(1, 'cardId', $pb.PbFieldType.OU3)
    ..hasRequiredFields = false
  ;

  UserCodeRequest._() : super();
  factory UserCodeRequest() => create();
  factory UserCodeRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserCodeRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  UserCodeRequest clone() => UserCodeRequest()..mergeFromMessage(this);
  UserCodeRequest copyWith(void Function(UserCodeRequest) updates) => super.copyWith((message) => updates(message as UserCodeRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserCodeRequest create() => UserCodeRequest._();
  UserCodeRequest createEmptyInstance() => create();
  static $pb.PbList<UserCodeRequest> createRepeated() => $pb.PbList<UserCodeRequest>();
  @$core.pragma('dart2js:noInline')
  static UserCodeRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserCodeRequest>(create);
  static UserCodeRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get cardId => $_getIZ(0);
  @$pb.TagNumber(1)
  set cardId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCardId() => $_has(0);
  @$pb.TagNumber(1)
  void clearCardId() => clearField(1);
}

class UserCodeResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('UserCodeResponse', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..aOS(1, 'userCode')
    ..hasRequiredFields = false
  ;

  UserCodeResponse._() : super();
  factory UserCodeResponse() => create();
  factory UserCodeResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserCodeResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  UserCodeResponse clone() => UserCodeResponse()..mergeFromMessage(this);
  UserCodeResponse copyWith(void Function(UserCodeResponse) updates) => super.copyWith((message) => updates(message as UserCodeResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserCodeResponse create() => UserCodeResponse._();
  UserCodeResponse createEmptyInstance() => create();
  static $pb.PbList<UserCodeResponse> createRepeated() => $pb.PbList<UserCodeResponse>();
  @$core.pragma('dart2js:noInline')
  static UserCodeResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserCodeResponse>(create);
  static UserCodeResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userCode => $_getSZ(0);
  @$pb.TagNumber(1)
  set userCode($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserCode() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserCode() => clearField(1);
}

class CreateUserCodeRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('CreateUserCodeRequest', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..a<$core.int>(1, 'cardId', $pb.PbFieldType.OU3)
    ..aOS(2, 'userCode')
    ..hasRequiredFields = false
  ;

  CreateUserCodeRequest._() : super();
  factory CreateUserCodeRequest() => create();
  factory CreateUserCodeRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateUserCodeRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  CreateUserCodeRequest clone() => CreateUserCodeRequest()..mergeFromMessage(this);
  CreateUserCodeRequest copyWith(void Function(CreateUserCodeRequest) updates) => super.copyWith((message) => updates(message as CreateUserCodeRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static CreateUserCodeRequest create() => CreateUserCodeRequest._();
  CreateUserCodeRequest createEmptyInstance() => create();
  static $pb.PbList<CreateUserCodeRequest> createRepeated() => $pb.PbList<CreateUserCodeRequest>();
  @$core.pragma('dart2js:noInline')
  static CreateUserCodeRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateUserCodeRequest>(create);
  static CreateUserCodeRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get cardId => $_getIZ(0);
  @$pb.TagNumber(1)
  set cardId($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCardId() => $_has(0);
  @$pb.TagNumber(1)
  void clearCardId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get userCode => $_getSZ(1);
  @$pb.TagNumber(2)
  set userCode($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserCode() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserCode() => clearField(2);
}

class Session extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Session', package: const $pb.PackageName('timeterm_user'), createEmptyInstance: create)
    ..aOS(1, 'id')
    ..a<$core.int>(2, 'cardId', $pb.PbFieldType.OU3)
    ..aOM<$2.Duration>(3, 'expiresIn', subBuilder: $2.Duration.create)
    ..hasRequiredFields = false
  ;

  Session._() : super();
  factory Session() => create();
  factory Session.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Session.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Session clone() => Session()..mergeFromMessage(this);
  Session copyWith(void Function(Session) updates) => super.copyWith((message) => updates(message as Session));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Session create() => Session._();
  Session createEmptyInstance() => create();
  static $pb.PbList<Session> createRepeated() => $pb.PbList<Session>();
  @$core.pragma('dart2js:noInline')
  static Session getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Session>(create);
  static Session _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get id => $_getSZ(0);
  @$pb.TagNumber(1)
  set id($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get cardId => $_getIZ(1);
  @$pb.TagNumber(2)
  set cardId($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasCardId() => $_has(1);
  @$pb.TagNumber(2)
  void clearCardId() => clearField(2);

  @$pb.TagNumber(3)
  $2.Duration get expiresIn => $_getN(2);
  @$pb.TagNumber(3)
  set expiresIn($2.Duration v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasExpiresIn() => $_has(2);
  @$pb.TagNumber(3)
  void clearExpiresIn() => clearField(3);
  @$pb.TagNumber(3)
  $2.Duration ensureExpiresIn() => $_ensure(2);
}


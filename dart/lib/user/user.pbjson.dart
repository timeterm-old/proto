///
//  Generated code. Do not modify.
//  source: user.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const SessionRequest$json = const {
  '1': 'SessionRequest',
  '2': const [
    const {'1': 'card_id', '3': 1, '4': 1, '5': 13, '10': 'cardId'},
  ],
};

const SessionResponse$json = const {
  '1': 'SessionResponse',
  '2': const [
    const {'1': 'session', '3': 1, '4': 1, '5': 11, '6': '.timeterm_user.Session', '10': 'session'},
  ],
};

const ValidateSessionRequest$json = const {
  '1': 'ValidateSessionRequest',
  '2': const [
    const {'1': 'session_id', '3': 1, '4': 1, '5': 9, '10': 'sessionId'},
  ],
};

const ValidateSessionResponse$json = const {
  '1': 'ValidateSessionResponse',
  '2': const [
    const {'1': 'valid', '3': 1, '4': 1, '5': 8, '10': 'valid'},
    const {'1': 'session', '3': 2, '4': 1, '5': 11, '6': '.timeterm_user.Session', '10': 'session'},
  ],
};

const UserCodeRequest$json = const {
  '1': 'UserCodeRequest',
  '2': const [
    const {'1': 'card_id', '3': 1, '4': 1, '5': 13, '10': 'cardId'},
  ],
};

const UserCodeResponse$json = const {
  '1': 'UserCodeResponse',
  '2': const [
    const {'1': 'user_code', '3': 1, '4': 1, '5': 9, '10': 'userCode'},
  ],
};

const CreateUserCodeRequest$json = const {
  '1': 'CreateUserCodeRequest',
  '2': const [
    const {'1': 'card_id', '3': 1, '4': 1, '5': 13, '10': 'cardId'},
    const {'1': 'user_code', '3': 2, '4': 1, '5': 9, '10': 'userCode'},
  ],
};

const Session$json = const {
  '1': 'Session',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'card_id', '3': 2, '4': 1, '5': 13, '10': 'cardId'},
    const {'1': 'expires_in', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Duration', '10': 'expiresIn'},
  ],
};


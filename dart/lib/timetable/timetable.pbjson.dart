///
//  Generated code. Do not modify.
//  source: timetable.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const AnnouncementsRequest$json = const {
  '1': 'AnnouncementsRequest',
  '2': const [
    const {'1': 'current', '3': 1, '4': 1, '5': 8, '10': 'current'},
    const {'1': 'tt_session_id', '3': 2, '4': 1, '5': 9, '10': 'ttSessionId'},
  ],
};

const AnnouncementsResponse$json = const {
  '1': 'AnnouncementsResponse',
  '2': const [
    const {'1': 'announcements', '3': 1, '4': 3, '5': 11, '6': '.timeterm_timetable.Announcement', '10': 'announcements'},
  ],
};

const Announcement$json = const {
  '1': 'Announcement',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'start', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'start'},
    const {'1': 'end', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'end'},
    const {'1': 'title', '3': 4, '4': 1, '5': 9, '10': 'title'},
    const {'1': 'text', '3': 5, '4': 1, '5': 9, '10': 'text'},
  ],
};

const AppointmentsRequest$json = const {
  '1': 'AppointmentsRequest',
  '2': const [
    const {'1': 'start', '3': 1, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'start'},
    const {'1': 'end', '3': 2, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'end'},
    const {'1': 'tt_session_id', '3': 3, '4': 1, '5': 9, '10': 'ttSessionId'},
  ],
};

const AppointmentsResponse$json = const {
  '1': 'AppointmentsResponse',
  '2': const [
    const {'1': 'appointments', '3': 1, '4': 3, '5': 11, '6': '.timeterm_timetable.Appointment', '10': 'appointments'},
  ],
};

const Appointment$json = const {
  '1': 'Appointment',
  '2': const [
    const {'1': 'appointment_instance', '3': 1, '4': 1, '5': 3, '10': 'appointmentInstance'},
    const {'1': 'id', '3': 2, '4': 1, '5': 3, '10': 'id'},
    const {'1': 'start', '3': 3, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'start'},
    const {'1': 'end', '3': 4, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'end'},
    const {'1': 'created', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'created'},
    const {'1': 'last_modified', '3': 6, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'lastModified'},
    const {'1': 'change_description', '3': 7, '4': 1, '5': 9, '10': 'changeDescription'},
    const {'1': 'start_time_slot', '3': 8, '4': 1, '5': 5, '10': 'startTimeSlot'},
    const {'1': 'end_time_slot', '3': 9, '4': 1, '5': 5, '10': 'endTimeSlot'},
    const {'1': 'subjects', '3': 10, '4': 3, '5': 9, '10': 'subjects'},
    const {'1': 'type', '3': 11, '4': 1, '5': 9, '10': 'type'},
    const {'1': 'remark', '3': 12, '4': 1, '5': 9, '10': 'remark'},
    const {'1': 'valid', '3': 13, '4': 1, '5': 8, '10': 'valid'},
    const {'1': 'hidden', '3': 14, '4': 1, '5': 8, '10': 'hidden'},
    const {'1': 'canceled', '3': 15, '4': 1, '5': 8, '10': 'canceled'},
    const {'1': 'modified', '3': 16, '4': 1, '5': 8, '10': 'modified'},
    const {'1': 'moved', '3': 17, '4': 1, '5': 8, '10': 'moved'},
    const {'1': 'new', '3': 18, '4': 1, '5': 8, '10': 'new'},
    const {'1': 'locations', '3': 19, '4': 3, '5': 9, '10': 'locations'},
    const {'1': 'teachers', '3': 20, '4': 3, '5': 9, '10': 'teachers'},
    const {'1': 'groups', '3': 21, '4': 3, '5': 9, '10': 'groups'},
  ],
};


///
//  Generated code. Do not modify.
//  source: timetable.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import '../google/protobuf/timestamp.pb.dart' as $1;

class AnnouncementsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AnnouncementsRequest', package: const $pb.PackageName('timeterm_timetable'), createEmptyInstance: create)
    ..aOB(1, 'current')
    ..aOS(2, 'ttSessionId')
    ..hasRequiredFields = false
  ;

  AnnouncementsRequest._() : super();
  factory AnnouncementsRequest() => create();
  factory AnnouncementsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AnnouncementsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AnnouncementsRequest clone() => AnnouncementsRequest()..mergeFromMessage(this);
  AnnouncementsRequest copyWith(void Function(AnnouncementsRequest) updates) => super.copyWith((message) => updates(message as AnnouncementsRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AnnouncementsRequest create() => AnnouncementsRequest._();
  AnnouncementsRequest createEmptyInstance() => create();
  static $pb.PbList<AnnouncementsRequest> createRepeated() => $pb.PbList<AnnouncementsRequest>();
  @$core.pragma('dart2js:noInline')
  static AnnouncementsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AnnouncementsRequest>(create);
  static AnnouncementsRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get current => $_getBF(0);
  @$pb.TagNumber(1)
  set current($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasCurrent() => $_has(0);
  @$pb.TagNumber(1)
  void clearCurrent() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get ttSessionId => $_getSZ(1);
  @$pb.TagNumber(2)
  set ttSessionId($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasTtSessionId() => $_has(1);
  @$pb.TagNumber(2)
  void clearTtSessionId() => clearField(2);
}

class AnnouncementsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AnnouncementsResponse', package: const $pb.PackageName('timeterm_timetable'), createEmptyInstance: create)
    ..pc<Announcement>(1, 'announcements', $pb.PbFieldType.PM, subBuilder: Announcement.create)
    ..hasRequiredFields = false
  ;

  AnnouncementsResponse._() : super();
  factory AnnouncementsResponse() => create();
  factory AnnouncementsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AnnouncementsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AnnouncementsResponse clone() => AnnouncementsResponse()..mergeFromMessage(this);
  AnnouncementsResponse copyWith(void Function(AnnouncementsResponse) updates) => super.copyWith((message) => updates(message as AnnouncementsResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AnnouncementsResponse create() => AnnouncementsResponse._();
  AnnouncementsResponse createEmptyInstance() => create();
  static $pb.PbList<AnnouncementsResponse> createRepeated() => $pb.PbList<AnnouncementsResponse>();
  @$core.pragma('dart2js:noInline')
  static AnnouncementsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AnnouncementsResponse>(create);
  static AnnouncementsResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Announcement> get announcements => $_getList(0);
}

class Announcement extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Announcement', package: const $pb.PackageName('timeterm_timetable'), createEmptyInstance: create)
    ..aInt64(1, 'id')
    ..aOM<$1.Timestamp>(2, 'start', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(3, 'end', subBuilder: $1.Timestamp.create)
    ..aOS(4, 'title')
    ..aOS(5, 'text')
    ..hasRequiredFields = false
  ;

  Announcement._() : super();
  factory Announcement() => create();
  factory Announcement.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Announcement.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Announcement clone() => Announcement()..mergeFromMessage(this);
  Announcement copyWith(void Function(Announcement) updates) => super.copyWith((message) => updates(message as Announcement));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Announcement create() => Announcement._();
  Announcement createEmptyInstance() => create();
  static $pb.PbList<Announcement> createRepeated() => $pb.PbList<Announcement>();
  @$core.pragma('dart2js:noInline')
  static Announcement getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Announcement>(create);
  static Announcement _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $1.Timestamp get start => $_getN(1);
  @$pb.TagNumber(2)
  set start($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasStart() => $_has(1);
  @$pb.TagNumber(2)
  void clearStart() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureStart() => $_ensure(1);

  @$pb.TagNumber(3)
  $1.Timestamp get end => $_getN(2);
  @$pb.TagNumber(3)
  set end($1.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasEnd() => $_has(2);
  @$pb.TagNumber(3)
  void clearEnd() => clearField(3);
  @$pb.TagNumber(3)
  $1.Timestamp ensureEnd() => $_ensure(2);

  @$pb.TagNumber(4)
  $core.String get title => $_getSZ(3);
  @$pb.TagNumber(4)
  set title($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTitle() => $_has(3);
  @$pb.TagNumber(4)
  void clearTitle() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get text => $_getSZ(4);
  @$pb.TagNumber(5)
  set text($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasText() => $_has(4);
  @$pb.TagNumber(5)
  void clearText() => clearField(5);
}

class AppointmentsRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AppointmentsRequest', package: const $pb.PackageName('timeterm_timetable'), createEmptyInstance: create)
    ..aOM<$1.Timestamp>(1, 'start', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(2, 'end', subBuilder: $1.Timestamp.create)
    ..aOS(3, 'ttSessionId')
    ..hasRequiredFields = false
  ;

  AppointmentsRequest._() : super();
  factory AppointmentsRequest() => create();
  factory AppointmentsRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AppointmentsRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AppointmentsRequest clone() => AppointmentsRequest()..mergeFromMessage(this);
  AppointmentsRequest copyWith(void Function(AppointmentsRequest) updates) => super.copyWith((message) => updates(message as AppointmentsRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AppointmentsRequest create() => AppointmentsRequest._();
  AppointmentsRequest createEmptyInstance() => create();
  static $pb.PbList<AppointmentsRequest> createRepeated() => $pb.PbList<AppointmentsRequest>();
  @$core.pragma('dart2js:noInline')
  static AppointmentsRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AppointmentsRequest>(create);
  static AppointmentsRequest _defaultInstance;

  @$pb.TagNumber(1)
  $1.Timestamp get start => $_getN(0);
  @$pb.TagNumber(1)
  set start($1.Timestamp v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStart() => $_has(0);
  @$pb.TagNumber(1)
  void clearStart() => clearField(1);
  @$pb.TagNumber(1)
  $1.Timestamp ensureStart() => $_ensure(0);

  @$pb.TagNumber(2)
  $1.Timestamp get end => $_getN(1);
  @$pb.TagNumber(2)
  set end($1.Timestamp v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasEnd() => $_has(1);
  @$pb.TagNumber(2)
  void clearEnd() => clearField(2);
  @$pb.TagNumber(2)
  $1.Timestamp ensureEnd() => $_ensure(1);

  @$pb.TagNumber(3)
  $core.String get ttSessionId => $_getSZ(2);
  @$pb.TagNumber(3)
  set ttSessionId($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasTtSessionId() => $_has(2);
  @$pb.TagNumber(3)
  void clearTtSessionId() => clearField(3);
}

class AppointmentsResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AppointmentsResponse', package: const $pb.PackageName('timeterm_timetable'), createEmptyInstance: create)
    ..pc<Appointment>(1, 'appointments', $pb.PbFieldType.PM, subBuilder: Appointment.create)
    ..hasRequiredFields = false
  ;

  AppointmentsResponse._() : super();
  factory AppointmentsResponse() => create();
  factory AppointmentsResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AppointmentsResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AppointmentsResponse clone() => AppointmentsResponse()..mergeFromMessage(this);
  AppointmentsResponse copyWith(void Function(AppointmentsResponse) updates) => super.copyWith((message) => updates(message as AppointmentsResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AppointmentsResponse create() => AppointmentsResponse._();
  AppointmentsResponse createEmptyInstance() => create();
  static $pb.PbList<AppointmentsResponse> createRepeated() => $pb.PbList<AppointmentsResponse>();
  @$core.pragma('dart2js:noInline')
  static AppointmentsResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AppointmentsResponse>(create);
  static AppointmentsResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Appointment> get appointments => $_getList(0);
}

class Appointment extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Appointment', package: const $pb.PackageName('timeterm_timetable'), createEmptyInstance: create)
    ..aInt64(1, 'appointmentInstance')
    ..aInt64(2, 'id')
    ..aOM<$1.Timestamp>(3, 'start', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(4, 'end', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(5, 'created', subBuilder: $1.Timestamp.create)
    ..aOM<$1.Timestamp>(6, 'lastModified', subBuilder: $1.Timestamp.create)
    ..aOS(7, 'changeDescription')
    ..a<$core.int>(8, 'startTimeSlot', $pb.PbFieldType.O3)
    ..a<$core.int>(9, 'endTimeSlot', $pb.PbFieldType.O3)
    ..pPS(10, 'subjects')
    ..aOS(11, 'type')
    ..aOS(12, 'remark')
    ..aOB(13, 'valid')
    ..aOB(14, 'hidden')
    ..aOB(15, 'canceled')
    ..aOB(16, 'modified')
    ..aOB(17, 'moved')
    ..aOB(18, 'new')
    ..pPS(19, 'locations')
    ..pPS(20, 'teachers')
    ..pPS(21, 'groups')
    ..hasRequiredFields = false
  ;

  Appointment._() : super();
  factory Appointment() => create();
  factory Appointment.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Appointment.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Appointment clone() => Appointment()..mergeFromMessage(this);
  Appointment copyWith(void Function(Appointment) updates) => super.copyWith((message) => updates(message as Appointment));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Appointment create() => Appointment._();
  Appointment createEmptyInstance() => create();
  static $pb.PbList<Appointment> createRepeated() => $pb.PbList<Appointment>();
  @$core.pragma('dart2js:noInline')
  static Appointment getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Appointment>(create);
  static Appointment _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get appointmentInstance => $_getI64(0);
  @$pb.TagNumber(1)
  set appointmentInstance($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasAppointmentInstance() => $_has(0);
  @$pb.TagNumber(1)
  void clearAppointmentInstance() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get id => $_getI64(1);
  @$pb.TagNumber(2)
  set id($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);

  @$pb.TagNumber(3)
  $1.Timestamp get start => $_getN(2);
  @$pb.TagNumber(3)
  set start($1.Timestamp v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasStart() => $_has(2);
  @$pb.TagNumber(3)
  void clearStart() => clearField(3);
  @$pb.TagNumber(3)
  $1.Timestamp ensureStart() => $_ensure(2);

  @$pb.TagNumber(4)
  $1.Timestamp get end => $_getN(3);
  @$pb.TagNumber(4)
  set end($1.Timestamp v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasEnd() => $_has(3);
  @$pb.TagNumber(4)
  void clearEnd() => clearField(4);
  @$pb.TagNumber(4)
  $1.Timestamp ensureEnd() => $_ensure(3);

  @$pb.TagNumber(5)
  $1.Timestamp get created => $_getN(4);
  @$pb.TagNumber(5)
  set created($1.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasCreated() => $_has(4);
  @$pb.TagNumber(5)
  void clearCreated() => clearField(5);
  @$pb.TagNumber(5)
  $1.Timestamp ensureCreated() => $_ensure(4);

  @$pb.TagNumber(6)
  $1.Timestamp get lastModified => $_getN(5);
  @$pb.TagNumber(6)
  set lastModified($1.Timestamp v) { setField(6, v); }
  @$pb.TagNumber(6)
  $core.bool hasLastModified() => $_has(5);
  @$pb.TagNumber(6)
  void clearLastModified() => clearField(6);
  @$pb.TagNumber(6)
  $1.Timestamp ensureLastModified() => $_ensure(5);

  @$pb.TagNumber(7)
  $core.String get changeDescription => $_getSZ(6);
  @$pb.TagNumber(7)
  set changeDescription($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasChangeDescription() => $_has(6);
  @$pb.TagNumber(7)
  void clearChangeDescription() => clearField(7);

  @$pb.TagNumber(8)
  $core.int get startTimeSlot => $_getIZ(7);
  @$pb.TagNumber(8)
  set startTimeSlot($core.int v) { $_setSignedInt32(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasStartTimeSlot() => $_has(7);
  @$pb.TagNumber(8)
  void clearStartTimeSlot() => clearField(8);

  @$pb.TagNumber(9)
  $core.int get endTimeSlot => $_getIZ(8);
  @$pb.TagNumber(9)
  set endTimeSlot($core.int v) { $_setSignedInt32(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasEndTimeSlot() => $_has(8);
  @$pb.TagNumber(9)
  void clearEndTimeSlot() => clearField(9);

  @$pb.TagNumber(10)
  $core.List<$core.String> get subjects => $_getList(9);

  @$pb.TagNumber(11)
  $core.String get type => $_getSZ(10);
  @$pb.TagNumber(11)
  set type($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasType() => $_has(10);
  @$pb.TagNumber(11)
  void clearType() => clearField(11);

  @$pb.TagNumber(12)
  $core.String get remark => $_getSZ(11);
  @$pb.TagNumber(12)
  set remark($core.String v) { $_setString(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasRemark() => $_has(11);
  @$pb.TagNumber(12)
  void clearRemark() => clearField(12);

  @$pb.TagNumber(13)
  $core.bool get valid => $_getBF(12);
  @$pb.TagNumber(13)
  set valid($core.bool v) { $_setBool(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasValid() => $_has(12);
  @$pb.TagNumber(13)
  void clearValid() => clearField(13);

  @$pb.TagNumber(14)
  $core.bool get hidden => $_getBF(13);
  @$pb.TagNumber(14)
  set hidden($core.bool v) { $_setBool(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasHidden() => $_has(13);
  @$pb.TagNumber(14)
  void clearHidden() => clearField(14);

  @$pb.TagNumber(15)
  $core.bool get canceled => $_getBF(14);
  @$pb.TagNumber(15)
  set canceled($core.bool v) { $_setBool(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasCanceled() => $_has(14);
  @$pb.TagNumber(15)
  void clearCanceled() => clearField(15);

  @$pb.TagNumber(16)
  $core.bool get modified => $_getBF(15);
  @$pb.TagNumber(16)
  set modified($core.bool v) { $_setBool(15, v); }
  @$pb.TagNumber(16)
  $core.bool hasModified() => $_has(15);
  @$pb.TagNumber(16)
  void clearModified() => clearField(16);

  @$pb.TagNumber(17)
  $core.bool get moved => $_getBF(16);
  @$pb.TagNumber(17)
  set moved($core.bool v) { $_setBool(16, v); }
  @$pb.TagNumber(17)
  $core.bool hasMoved() => $_has(16);
  @$pb.TagNumber(17)
  void clearMoved() => clearField(17);

  @$pb.TagNumber(18)
  $core.bool get new_18 => $_getBF(17);
  @$pb.TagNumber(18)
  set new_18($core.bool v) { $_setBool(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasNew_18() => $_has(17);
  @$pb.TagNumber(18)
  void clearNew_18() => clearField(18);

  @$pb.TagNumber(19)
  $core.List<$core.String> get locations => $_getList(18);

  @$pb.TagNumber(20)
  $core.List<$core.String> get teachers => $_getList(19);

  @$pb.TagNumber(21)
  $core.List<$core.String> get groups => $_getList(20);
}


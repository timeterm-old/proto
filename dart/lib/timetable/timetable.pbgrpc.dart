///
//  Generated code. Do not modify.
//  source: timetable.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'timetable.pb.dart' as $0;
export 'timetable.pb.dart';

class TimetableClient extends $grpc.Client {
  static final _$announcements =
      $grpc.ClientMethod<$0.AnnouncementsRequest, $0.AnnouncementsResponse>(
          '/timeterm_timetable.Timetable/Announcements',
          ($0.AnnouncementsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AnnouncementsResponse.fromBuffer(value));
  static final _$appointments =
      $grpc.ClientMethod<$0.AppointmentsRequest, $0.AppointmentsResponse>(
          '/timeterm_timetable.Timetable/Appointments',
          ($0.AppointmentsRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.AppointmentsResponse.fromBuffer(value));

  TimetableClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.AnnouncementsResponse> announcements(
      $0.AnnouncementsRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$announcements, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.AppointmentsResponse> appointments(
      $0.AppointmentsRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$appointments, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class TimetableServiceBase extends $grpc.Service {
  $core.String get $name => 'timeterm_timetable.Timetable';

  TimetableServiceBase() {
    $addMethod(
        $grpc.ServiceMethod<$0.AnnouncementsRequest, $0.AnnouncementsResponse>(
            'Announcements',
            announcements_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.AnnouncementsRequest.fromBuffer(value),
            ($0.AnnouncementsResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.AppointmentsRequest, $0.AppointmentsResponse>(
            'Appointments',
            appointments_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.AppointmentsRequest.fromBuffer(value),
            ($0.AppointmentsResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.AnnouncementsResponse> announcements_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.AnnouncementsRequest> request) async {
    return announcements(call, await request);
  }

  $async.Future<$0.AppointmentsResponse> appointments_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.AppointmentsRequest> request) async {
    return appointments(call, await request);
  }

  $async.Future<$0.AnnouncementsResponse> announcements(
      $grpc.ServiceCall call, $0.AnnouncementsRequest request);
  $async.Future<$0.AppointmentsResponse> appointments(
      $grpc.ServiceCall call, $0.AppointmentsRequest request);
}

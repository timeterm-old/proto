PROTO_ROOT_DIR ?= /usr/include

rwildcard = $(foreach d,$(wildcard $1*),$(call rwildcard, $d/, $2) $(filter $(subst *,%,$2),$d))

PROTO_SOURCES := $(call rwildcard,,*.proto)
PROTO_GO_DEPS := $(PROTO_SOURCES:%.proto=%.pb.go)
PROTO_DART_DEPS := $(PROTO_SOURCES:%.proto=%.pb.dart)

%.pb.go: %.proto
	mkdir -p $(subst src/timeterm_proto,go,$(dir $<))
	protoc -I=$(dir $<) \
		-I=$(GOPATH)/src/github.com/gogo/protobuf/protobuf \
		-I=$(GOPATH)/src \
		--gogoslick_out=\
plugins=grpc,\
Mgoogle/protobuf/any.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/duration.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/empty.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/struct.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/timestamp.proto=github.com/gogo/protobuf/types,\
Mgoogle/protobuf/wrappers.proto=github.com/gogo/protobuf/types:\
$(subst src/timeterm_proto,go,$(dir $<)) \
		$<

.PHONY: proto-dart-base
proto-dart-base:
	mkdir -p $(subst src/timeterm_proto,dart/lib,dart/lib)
	protoc -I=$(PROTO_ROOT_DIR) \
		--dart_out=grpc:dart/lib \
		$(PROTO_ROOT_DIR)/google/protobuf/*.proto

.PHONY: %.pb.dart
%.pb.dart: %.proto
	mkdir -p $(subst src/timeterm_proto,dart/lib,$(dir $<))
	protoc -I=$(dir $<) -I=$(PROTO_ROOT_DIR) \
		--dart_out=grpc:\
$(subst src/timeterm_proto,dart/lib,$(dir $<)) \
		$<

.PHONY: proto-go
proto-go: $(PROTO_GO_DEPS)

.PHONY: proto-dart
proto-dart: proto-dart-base $(PROTO_DART_DEPS)
	find dart/lib -type f -name "*.dart" -print0 | xargs -0 sed -i'' -e 's/google\/protobuf\//..\/google\/protobuf\//g'

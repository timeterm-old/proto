from conans import ConanFile, CMake


class TimetermprotoConan(ConanFile):
    name = "TimetermProto"
    version = "0.1"
    author = "Rutger Broekhoff"
    url = "https://gitlab.com/timeterm/proto"
    description = "Protobuf generated code for Timeterm"
    topics = ("protobuf", "rpc")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"
    exports_sources = "src*"
    requires = "grpc/1.25.0@inexorgame/stable"
    license = "BSD-3"

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="src")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", keep_path=True)
        self.copy("*.hpp", dst="include", keep_path=True)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["timeterm_proto"]
